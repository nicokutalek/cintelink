import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from "@/store";
import auth from "@/mixins/auth";
import VueRouter from 'vue-router'

import './assets/sass/styles.scss';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUnlock, faExclamationCircle, faRightFromBracket } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate';
import { min, max } from 'vee-validate/dist/rules';

//FontAwsome incons
library.add(faUnlock, faRightFromBracket, faExclamationCircle)

// Rule.
extend('validcaracter', {
    validate: value => /^[a-zA-Z0-9.]+$/.test(value),
    message: 'Solo puede contener letras, números o puntos.'
});
extend('min', {...min, message:'El nombre de usuario debe tener entre 3 y 20 caracteres.'});
extend('minpass', {...min, message:'La contraseña debe tener al menos 8 caracteres.'});
extend('max', {...max, message:'El nombre de usuario debe tener entre 3 y 20 caracteres.'});
extend('required', {
    validate(value) {
        return {
            required: true,
            valid: ['', null, undefined].indexOf(value) === -1
        };
    },
    message: 'Rellene el campo.',
    computesRequired: true
});

// Globally components
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.mixin(auth)
Vue.use(VueRouter)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
