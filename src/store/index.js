import Vue from 'vue';
import Vuex from 'vuex';

import mutations from './mutations';
import getters from './getters';
import actions from './actions';

Vue.use(Vuex);

const store = new Vuex.Store(
    {
        state: {
            user: null,
            token: localStorage.getItem('AccessToken'),
            logged: !!localStorage.getItem('AccessToken'),
        },
        mutations,
        getters,
        actions,
    },
);

export default store;
