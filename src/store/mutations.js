const mutations = {
    user(state, data) {
        state.user = data;
    },
    token(state, data) {
        state.token = data;
    },
    logged(state, data) {
        state.logged = data;
    },
}
export default mutations;
