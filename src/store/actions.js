const actions = {
    setUser(context) {
        const user = JSON.parse(localStorage.getItem('User'));
        const token = localStorage.getItem('AccessToken');
        if (user && token) {
            context.commit('user',
                {
                    id: user.id,
                    name: user.name,
                });
            context.commit('logged', true);
        } else {
            context.commit('user',
                {
                    user: null,
                });
            context.commit('logged', false);
        }
    },
}
export default actions
