const getters = {
    user: (state) => state.user,
    logged: (state) => state.logged,
};

export default getters;
