import axios from 'axios';

const API_URL = process.env.VUE_APP_API_URL;

export default {
    methods: {
        login(user, pass) {
            return new Promise((resolve, reject) => {
                axios.post(API_URL+'/login.mod.php', {
                    user: user,
                    pass: pass,
                    "accion":"LoginWSUser",
                    "format":"json_object"
                },{ headers : {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    }
                }).then((result) => {
                    let res = result.data;
                    if (res.error===0){
                        let data = result.data.data[0]
                        let token = data.token
                        let user = {
                            id: data.id_usuario,
                            name: data.nombre_completo
                        }
                        const user_json = JSON.stringify(user);
                        localStorage.setItem('AccessToken', token);
                        this.$store.commit('token', token);
                        localStorage.setItem('User', user_json);
                        this.$store.dispatch('setUser');
                    }
                    resolve(res);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
        logOut() {
            localStorage.removeItem('AccessToken');
            localStorage.removeItem('User');
            this.$store.dispatch('setUser');
            this.$router.push({name: 'login'})
        },
    },
};
