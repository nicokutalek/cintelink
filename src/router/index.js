import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginView from "@/views/LoginView";
import HomeView from "@/views/HomeView";
import NotFound from "@/views/NotFound";

import store from '@/store';

Vue.use(VueRouter)
const AuthGuard = (to, from, next) => {
  if (!store.getters.logged) {
    const query = (to.path !== '/') ? { redirect_to: to.path } : null;
    return next({ path: '/login', query });
  }
  next();
};
const routes = [
  {
    name: 'login',
    meta:
        {
          title: 'Ingresar',
        },
    path: '/login',
    component: LoginView,
    props: true,
  },
  {
    name: 'home',
    meta:
        {
          title: 'Inicio',
        },
    path: '/',
    component: HomeView,
    beforeEnter: AuthGuard,
  },
  {
    name: '404',
    meta:
        {
          title: 'No encontrado',
        },
    path: '/404',
    component: NotFound,
  },
]


const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  // Handling 404 errors
  if (!to.matched[0]) {
    return next({path:'/404'});
  }
  next();
});
router.afterEach((to) => {
  // Set title
  document.title = `${to.meta.title} - Cintelink`;
  // Set classes
  let className = to.meta.bodyClass || to.meta.title;
  className = className && className.replace(' ', '-').toLowerCase();

  document.getElementsByTagName('body')[0].className = className;
  document.getElementsByTagName('html')[0].className = className;

});

export default router
