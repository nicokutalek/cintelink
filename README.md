# Cintelink
## Pasos a seguir para realizar el build y deploy

### Project setup
```
npm install
```
### Se debe copiar el archivo .env en el directorio principal
### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
